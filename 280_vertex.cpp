
/*
UVA 280
Author: rakibdana@gmail.com/rakibsr@gmail.com
*/

#include <iostream>
using namespace std;

int nodes;
int g[500][500];
int v[200];
int ans[500][500] = {0};
int linked[200][200] = {0};
int babaSrc;
int numOutput = 0;

void printG()
{
	int i = 0, j = 0;
	for (i = 1; i <= nodes; i++) {
		for (j = 1; j <= nodes; j++) {
			cout << g[i][j] << " ";
		}
		cout << "\n";
	}
}

void printAns()
{
	int i = 0;
	int nodeXcount = 0;
	while (i < 500) {
		nodeXcount = ans[i][0];
		if (nodeXcount > 0) {
			cout << nodeXcount << " ";
			int j = 1;
			while (j <= nodeXcount) {
				cout << ans[i][j] << " " ;
				j++;
			}	
			cout << "\n";
		} 
		if (nodeXcount == 0){
			cout << nodeXcount << " \n";
		}
		i++;
	}
}
void getAns()
{
	int i = 0;
	int val = 1;

	for (i = 1; i <=nodes; i++) {
	
		if (linked[babaSrc][i] == 0) {
		
			ans[numOutput][val] = i;
				
			val++;
		}
	
	}
	ans[numOutput][0] = val - 1;
	numOutput++;
}

void dfs_visit(int aNode)
{
	int i;
	//v[aNode] = 1; 
	for (i = 1; i <= nodes ; i++) {
		if (g[aNode][i] && !v[i]) {
			v[i] = 1; //visited
			linked[babaSrc][i] = 1;
			dfs_visit(i);
		}
	}
}

void dfs(int source)
{
	int i;
	for (i = 1; i <= nodes; i++) {
		v[i] = 0;
	}
	babaSrc = source;
	dfs_visit(source);
}

void init() 
{
	int i = 0, j = 0;
	for (i = 1; i <= nodes; i++) {
		for (j = 1; j <= nodes; j++) {
			g[i][j] = 0;
			linked[i][j] = 0;
		}
	}
}

void initAns() 
{
	int i = 0, j = 0;
	for (i = 0; i < 500; i++) {
		for (j = 0; j < 500; j++) {
			ans[i][j] = -1;
		}
	}
}

int main () 
{
	int sNode = 0, nNode = 0, qNodes = 0;
	int i = 0;

	initAns();

	while (cin >> nodes) {		
		//cin >> nodes;
		if (!nodes) break;
		init();
		while (1) {
			cin >> sNode;
			if (!sNode) break;

			while (1) {
				cin >> nNode;
				if (!nNode) break;
				g[sNode][nNode] = 1;
				sNode = nNode;
			}
		}
		cin >> qNodes;
		int qNode;
		for (i = 0; i < qNodes; i++) {
			cin >> qNode;
			dfs(qNode);
			getAns();
		}	
			//printG();
	}

	printAns();

	return 0;
}